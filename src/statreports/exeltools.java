package statreports;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;

public class exeltools {
    static JSONObject jsonInput = null;
    //gets JSON result for parsing to exel report
    public static void getJsonInput(JSONObject input){
        jsonInput = input;
    }
    //apache.poi testing
    public static void formTestReport() throws IOException {
        Workbook book = new HSSFWorkbook();
        Sheet sheet = book.createSheet();
        Row row = sheet.createRow(0);

        Cell testCell = row.createCell(0);
        testCell.setCellValue("TEST");

        book.write(new FileOutputStream("OutReport.xls"));
        book.close();
    }
    //main report forming method
    public static void formShortReport() throws IOException {
        JSONObject queryDetails;
        String qDetails;
        Workbook book = new HSSFWorkbook();
        Sheet sheet = book.createSheet();
        Sheet sheet1 = book.createSheet();

        book.setSheetName(0, "Яндекс.Метрика");
        book.setSheetName(1, "Google Analytics");
        //init 20 rows of table to fill it with content
        int rowsCount = 20;
        Row rows[] = new Row[rowsCount];
        for (int i = 0; i < rowsCount; i++) {
            rows[i] = sheet.createRow(i);
        }
        //init cells in rows to fill it with names for numbers in report
        int nameCellsCount = 10;
        Cell nameCells[] = new Cell[nameCellsCount];
        for (int i = 0; i < nameCellsCount; i++) {
            nameCells[i] = rows[4+i].createCell(0);
        }
        //init cell to fill it with data from JSON
        int dataCellsCount = 10;
        //cells for total values
        Cell dataCellsTotal[] = new Cell[dataCellsCount];
        for (int i = 0; i < dataCellsCount; i++) {
            dataCellsTotal[i] = rows[4+i].createCell(1);
        }
        //cells for min values
        Cell dataCellsMin[] = new Cell[dataCellsCount];
        for (int i = 0; i < dataCellsCount; i++) {
            dataCellsMin[i] = rows[4+i].createCell(2);
        }
        //cells for max values
        Cell dataCellsMax[] = new Cell[dataCellsCount];
        for (int i = 0; i < dataCellsCount; i++) {
            dataCellsMax[i] = rows[4+i].createCell(3);
        }
        //some info cells
        Cell contentsCell = rows[0].createCell(0);
        Cell contentsCellDate = rows[1].createCell(0);

        Cell contentMinCell = rows[3].createCell(2);
        Cell contentMaxCell = rows[3].createCell(3);
        Cell contentTotalCell = rows[3].createCell(1);
        //getting some query info from JSON to set time period of analytics
        queryDetails = new JSONObject(jsonInput.get("query").toString());
        contentsCell.setCellValue("Отчет Яндекс.Метрики");
        contentsCellDate.setCellValue("Период статистики: " + queryDetails.get("date1") + " - " + queryDetails.get("date2"));
        contentMinCell.setCellValue("Min. ");
        contentMaxCell.setCellValue("Max. ");
        contentTotalCell.setCellValue("Total");
        //init data arrays
        String[] dataMinArray = new String[6];
        String[] dataMaxArray = new String[6];
        String[] dataTotalArray = new String[6];
        //fill data arrays from JSON
        for (int i = 0; i < 6; i++) {
            dataMinArray[i] = jsonInput.getJSONArray("min").toList().toArray()[i].toString();
            dataMaxArray[i] = jsonInput.getJSONArray("max").toList().toArray()[i].toString();
            dataTotalArray[i] = jsonInput.getJSONArray("totals").toList().toArray()[i].toString();
        }
        //set name cells
        nameCells[0].setCellValue("Визиты");
        nameCells[1].setCellValue("Просмотры");
        nameCells[2].setCellValue("Уникальные пользователи");
        nameCells[3].setCellValue("Показатель отказов");
        nameCells[4].setCellValue("Глубина просмотра");
        nameCells[5].setCellValue("Средняя продолжительность визита");
        //fill data cell with numbers from data arrays
        for (int i = 0; i < 6; i++) {
            dataCellsTotal[i].setCellValue(dataTotalArray[i]);
            dataCellsMin[i].setCellValue(dataMinArray[i]);
            dataCellsMax[i].setCellValue(dataMaxArray[i]);
        }
        //formatting cells to resize by content length
        for (int i = 0; i < rowsCount; i++) {
            sheet.autoSizeColumn(i);
        }
        //writing table to file
        book.write(new FileOutputStream("OutReport.xls"));
        book.close();
    }
}