package statreports;

import java.io.IOException;
import java.net.URL;

public class main {
    public static void main(String[] args){
        nettools.initMetricsMaps();

        //Right request: https://api-metrika.yandex.ru/stat/v1/data?id=2138128&metrics=ym:s:users&limit=50&oauth_token=05dd3dd84ff948fdae2bc4fb91f13e22bb1f289ceef0037
        //Counter number: 47211789
        //Token: AQAAAAAMoYbkAAS92yw0iN-4_kJOmAvRiZTmXRg
        //https://api-metrika.yandex.ru/stat/v1/data?id=47211789&metrics=ym:s:visits,ym:s:pageviews,ym:s:users&dimensions=ym:s:date&date1=7daysAgo&date2=yesterday&sort=ym:s:date&limit=50&oauth_token=AQAAAAAMoYbkAAS92yw0iN-4_kJOmAvRiZTmXRg
        String[] metricsArray = new String[6];
        metricsArray[0] = "visitsCount";
        metricsArray[1] = "pageViewsCount";
        metricsArray[2] = "uniqueUsersCount";
        metricsArray[3] = "bounceRate";
        metricsArray[4] = "pageViewDepth";
        metricsArray[5] = "avrVisitDuration";

        try {
            URL requestUrl = nettools.buildRequestUrl(metricsArray, "2018-01-04" , "today","47211789", "AQAAAAAMoYbkAAS92yw0iN-4_kJOmAvRiZTmXRg");
            nettools.sendRequest(requestUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Full JSON response:\n" + nettools.getJsonResult());


        try {
            exeltools.getJsonInput(nettools.getJsonResult());
            exeltools.formShortReport();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
