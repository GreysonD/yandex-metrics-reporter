package statreports;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class nettools {

    static Map<String,String> metricsTypes = new HashMap<String, String>();
    static Map<String,String> groupingTypes = new HashMap<String, String>();
    static JSONObject jsonResult = null;

    //metrics map initialization
    public static void initMetricsMaps(){
        //metrics initialization
        metricsTypes.put("visitsCount","ym:s:visits");
        metricsTypes.put("pageViewsCount","ym:s:pageviews");
        metricsTypes.put("uniqueUsersCount","ym:s:users");
        metricsTypes.put("bounceRate","ym:s:bounceRate");
        metricsTypes.put("pageViewDepth","ym:s:pageDepth");
        metricsTypes.put("avrVisitDuration","ym:s:avgVisitDurationSeconds");
        //grouping initialization - if it will be needed (?)

    }
    /*
    * Date format: YYYY-MM-DD
    * can use these date phrases:
    * today, yesterday, ndaysAgo (where n - days count)
    * */
    //Method for building right-formatted URL to send API request
    public static URL buildRequestUrl(String[] metricsArray, String startDate, String endDate, String id, String oauthToken) throws MalformedURLException {
        String resultStringUrl = "https://api-metrika.yandex.ru/stat/v1/data?";
        String metrics = "";
        for (int i = 0; i < metricsArray.length; i++) {
            metrics += metricsTypes.get(metricsArray[i]) + ",";
        }
        resultStringUrl += "&metrics=" + metrics.substring(0, metrics.length() - 1) + "&date1" + startDate + "&date2" + endDate + "&ids=" + id + "&oauth_token=" + oauthToken;

        URL resultUrl = new URL(resultStringUrl);
        System.out.println("Final request URL:\n" + resultStringUrl);

        return resultUrl;
    }

    public static void sendRequest(URL requestUrl) throws IOException { ;
        HttpURLConnection connection;
        InputStream stream;
        connection = null;

        String result = "not connected";

        connection = (HttpURLConnection)requestUrl.openConnection();
        connection.connect();

        stream = connection.getInputStream();
        int responseCode = connection.getResponseCode();
        ByteArrayOutputStream baosOutput = new ByteArrayOutputStream();
        if (responseCode == 200) {
            stream = connection.getInputStream();
            byte[] bufferB = new byte[8192];
            int readBytes = 0;
            while ((readBytes = stream.read(bufferB)) != -1) {
                baosOutput.write(bufferB, 0, readBytes);
            }
            result = new String(baosOutput.toByteArray(), "UTF-8");
            jsonResult = new JSONObject(result);
        }
        connection.disconnect();
    }

    //return request result in JSON
    public static JSONObject getJsonResult() {
        return  jsonResult;
    }
}